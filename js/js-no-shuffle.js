// Keeping out of global scope
(function() {

	// Elements
	// ===
	// I put each var on it's own line with declaration for readability, uglify will combine nicely.
	var choices = document.querySelectorAll('.choice');
	var clips = document.querySelectorAll('.clip');
	var clipImages = document.querySelectorAll('.clip-frame');
	var resetButton = document.querySelector('.reset');
	var allChoices = document.querySelector('.choices');
	var allClips = document.querySelector('.clips');
	var results = document.querySelector('.results');
	var countCorrect = document.querySelector('.count');
	var percentage = document.querySelector('.percentage');
	var winner = document.querySelector('.winner');
	var videoContainer = document.querySelector('.video-container');
	var videoPlayer = document.getElementById('video');

	// Placeholders
	// ===

	// Which clip is being dragged right now
	var activeClip = null;

	// How many items have you dragged? 
	var choicesMade = 0;

	// How many have you gotten correct? 
	var numberCorrect = 0;

	// Event functions
	// ===

	// When you start dragging...
	var dragItemStart = function(e) {

		// Set this clip as currently dragged
		activeClip = this;

		// Set the src to jpg
		var img = activeClip.querySelector('img');
		var imgSrc = img.getAttribute('src');
		var newSrc = imgSrc.replace('gif', 'jpg');
		img.setAttribute('src', newSrc);

		// set dataTransfer properties
		// ===

		// We're only moving stuff
		e.dataTransfer.effectAllowed = 'move';

		// grabbing the element
		e.dataTransfer.setData('text/html', this.innerHTML);

		// Grabbing the static image per instruction
		e.dataTransfer.setDragImage(this.querySelector('img'), 100, 57);

	}; // dragItemStart()

	// When you let go of your drag, without leaving it in a drop zone
	var dragItemEnd = function() {

		// Just clear the currently dragged item
		activeClip = null;

	}; // dragItemEnd()

	// When you're over a drop zone
	var dragItemOver = function(e) {

		if (e.preventDefault) {
			e.preventDefault();
		}

		// get ready to move
		e.dataTransfer.dropEffect = 'move';

		return false;

	}; // dragItemOver()

	// When you enter a drop zone
	var dragItemEnter = function() {

		// Light it up
		this.className = 'choice over';

	}; // dragItemEnter()

	// When you leave a drop zone
	var dragItemLeave = function() {

		// Shut it down
		this.className = 'choice';

	}; // dragItemLeave()

	// When you drop your item in a valid zone
	var dropItem = function(e) {

		if (e.preventDefault) {
			e.preventDefault();
		}

		if (e.stopPropagation) {
			e.stopPropagation();
		}

		// Force the class to locked on your source item
		activeClip.className = 'clip locked';

		// Mark this step taken
		this.className = 'choice chosen';

		// Replace the text with the newly dropped li (with locked classname)
		this.innerHTML = activeClip.outerHTML;

		// Change the source classname back to chosen
		activeClip.className = 'clip chosen';

		// We don't want to drag this again
		activeClip.setAttribute('draggable', 'false');

		// See how you did
		checkScore(this);

		var img = activeClip.querySelector('img');

		// Clear the source item
		activeClip = null;

		// Stop listening for drops in this space
		removeChoiceEvents(this);
		removeHoverEvents(img);

		return false;

	}; // dropItem()

	// When you hover over a clip
	var hoverOver = function() {

		// Give a little flash to the UI
		this.className = 'clip-frame animated';

		// Grab the src and change jpg to gif
		var imgSrc = this.getAttribute('src');
		var newSrc = imgSrc.replace('jpg', 'gif');
		this.setAttribute('src', newSrc);

	}; // hoverOver

	// When you hover off a clip
	var hoverOut = function() {

		// Reset the flash
		this.className = 'clip-frame';

		// Grab the src and change gif back to jpg
		var imgSrc = this.getAttribute('src');
		var newSrc = imgSrc.replace('gif', 'jpg');
		this.setAttribute('src', newSrc);

	}; // hoverOut

	// Utility functions
	// ===

	// Preload gifs
	var preloadGifs = function() {

		// List o' gifs
		var gifs = [
			'img/lemon_step_1_master.gif',
			'img/lemon_step_2_master.gif',
			'img/lemon_step_3_master.gif',
			'img/lemon_step_4_master.gif',
			'img/lemon_step_5_master.gif',
			'img/lemon_step_6_master.gif',
			'img/lemon_step_7_master.gif'
		];

		// New array
		var preloaded = [];

		// For each gif
		for (var i = 0; i < gifs.length; i++) {

			// Create a a new image and give it a path
			preloaded[i] = new Image();
			preloaded[i].src = gifs[i];

		}

	}; // preloadGifs
	

	// Shuffle the clips
	var shuffle = function(elem) {

		// All credit http://stackoverflow.com/a/11972692
		for (var i = elem.children.length; i >= 0; i--) {
			elem.appendChild(elem.children[Math.random() * i | 0]);
		}

	}; // shuffle()

	// Is your selection correct? 
	var checkScore = function(elem) {

		// Get the value of the dragged item
		var clipValue = activeClip.getAttribute('data-clip');

		// Get the value of the drop position
		var choiceValue = elem.getAttribute('data-choice');

		// If they match, the answer is correct
		if (clipValue === choiceValue) {

			// So tell the box about it
			elem.className = 'choice chosen correct';

			// And add it to your score
			numberCorrect++;

		} else {

			// Tell the box you got it wrong
			elem.className = 'choice chosen error';

		}

		// Add it to your answer count
		choicesMade++;

		// If you're answered all of them, it's time to finish the game
		if (choicesMade === 7) {
			completeGame();
		}

	}; // checkScore()

	// Get the final score
	var completeGame = function() {

		// What's  your score?
		var gamePercentage = Math.floor((numberCorrect / choicesMade) * 100);

		// Green and red borders can come out now
		allChoices.className = 'choices complete';

		// Score results can come out, too
		results.className = 'results visible';

		// Put in the number of correct guesses
		countCorrect.innerHTML = numberCorrect;

		// And the score
		percentage.innerHTML = gamePercentage;

		// Commented out due to no api connection
		// LMS.setScore(gamePercentage);

		if (gamePercentage > 80) {

			// Commented out due to no api connection
			// LMS.setStatus('complete');

			// Grey out the placeholders
			allChoices.className = 'choices complete aced';

			// You're doneski
			winner.innerHTML = "This assignment is now complete.";

			// Remove title card and play video
			videoContainer.className = 'video-container playing';
			videoPlayer.play();

		} else {

			// Try again
			winner.innerHTML = "Please try again.";

			// Commented out due to no api connection
			// LMS.setStatus('incomplete');

		}

		

	}; // completeGame()

	// Wanna start over? 
	var resetGame = function(e) {

		if (e.preventDefault) {
			e.preventDefault();
		}

		if (e.stopPropagation) {
			e.stopPropagation();
		}

		// Reset placeholder variables
		activeClip = null;
		choicesMade = 0;
		numberCorrect = 0;

		// Undo class and innerHTML changes
		allChoices.className = 'choices';
		results.className = 'results';
		countCorrect.innerHTML = '0';
		percentage.innerHTML = '0';
		winner.innerHTML = '';

		// Look through all the clips
		for (var i = 0; i < clips.length; i++) {

			// Kill old events for safety
			removeClipEvents(clips[i]);

			// And add them back
			addClipEvents(clips[i]);

			// Make them draggable again
			clips[i].setAttribute('draggable', 'true');

			// And clear the class names
			clips[i].className = 'clip';

		}

		// Look through all the choices
		for (var i = 0; i < choices.length; i++) {

			// replace the clips with step numbers
			choices[i].innerHTML = 'Step ' + (i + 1);

			// Kill old events for safety
			removeChoiceEvents(choices[i]);

			// And add them back
			addChoiceEvents(choices[i]);

			// And clear the class names
			choices[i].className = 'choice';

		}

		// Look through all the clip Images
		for (var i = 0; i < clipImages.length; i++) {

			// Kill old events for safety
			removeHoverEvents(clipImages[i]);

			// And add them back
			addHoverEvents(clipImages[i]);

		}

		// Reshuffle the video clips
		shuffle(allClips);

		// Stop video and reset to beginning, show title card
		videoPlayer.pause();
		videoPlayer.currentTime = 0;
		videoPlayer.load();
		videoContainer.className = 'video-container';

		return false;

	}; // resetGame()

	// Event Listeners
	// ===

	// All clip events
	var addClipEvents = function(elem) {
		elem.addEventListener('dragstart', dragItemStart);
		elem.addEventListener('dragend', dragItemEnd);
	};

	// Kill clip events
	var removeClipEvents = function(elem) {
		elem.removeEventListener('dragstart', dragItemStart);
		elem.removeEventListener('dragend', dragItemEnd);
	};

	// All choice events
	var addChoiceEvents = function(elem) {
		elem.addEventListener('dragover', dragItemOver);
		elem.addEventListener('dragenter', dragItemEnter);
		elem.addEventListener('dragleave', dragItemLeave);
		elem.addEventListener('drop', dropItem);
	};

	// Kill choice events
	var removeChoiceEvents = function(elem) {
		elem.removeEventListener('dragover', dragItemOver);
		elem.removeEventListener('dragenter', dragItemEnter);
		elem.removeEventListener('dragleave', dragItemLeave);
		elem.removeEventListener('drop', dropItem);
	};

	// All hover events
	var addHoverEvents = function(elem) {
		elem.addEventListener('mouseover', hoverOver);
		elem.addEventListener('mouseout', hoverOut);
	};

	// Kill hover events
	var removeHoverEvents = function(elem) {
		elem.removeEventListener('mouseover', hoverOver);
		elem.removeEventListener('mouseout', hoverOut);
	};

	// On pageload
	(function init() {

		// Shuffle the clips
		// shuffle(allClips);

		// Preload hovers
		preloadGifs();

		// Look through each clip
		for (var i = 0; i < clips.length; i++) {

			// And add events to all of them
			addClipEvents(clips[i]);

		}

		// Look through each choice
		for (var i = 0; i < choices.length; i++) {

			// And add events to all of them
			addChoiceEvents(choices[i]);

		}

		// Look through each image
		for (var i = 0; i < clipImages.length; i++) {

			// And add hover events to all of them
			addHoverEvents(clipImages[i]);

		}

		// Listen to the reset button
		resetButton.addEventListener('click', resetGame);

	})(); // init()
	
})();


























